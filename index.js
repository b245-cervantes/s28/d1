//CRUD Operations
  // CRUD operation is the heart of any backend application
  //mastering the CRUD operation is essential for any developer
  //This helps in building character and increasing exposure to lgogical statements
// that will help us manipulate
//mmastering the CRUD operations of any languages make us a valuable developer and makes the work easier for us
// to deal with huge amount of information.



//[Section] Create (Inserting document);
    //Since mongoDB deals with object as its structure
    //for documents, we can easily create them by providing 
    //objects into our methods.

    //mongoDB shell also uses javascript for it's for it's syntax which makes it convenient for us to understand
    // it's code.

    //Insert One document
       
       /*


           syntax:

           db.collectionName.insertOne({
	             object/document
           })


       */

      db.users.insertOne({
      	firstName : "Jane",
      	lastName: "Doe",
      	age: 21,
      	contact: {
      		phone: "1234567890",
      		email: "jandedoe@gmail.com"
      	},
      	course: ["CSS", "Javascript", "Python"],
      	department: "none"
      });

      //Insert many

      /*
          db.usesrs.insertMany([objectA,{objectB}])


      */

      db.users.insertMany([{
          firstName: "Stephen",
          lastName: "Hawking",
          age:76,
          contact: {
          	phone: "87654321",
          	email: "stepehenhawking@gmail.com"
          },
          courses: ["Python", "React", "PHP"],
          department: "none"
      },  {
          firstName: "Neil",
          lastName: "Armstrong",
          age: 82,
          contact: {
          	phone: "123456",
          	email: "armstrong@gmail.com"
          },
          courses: ["Laraval", "React", "Sass"],
          department: "none"
      }
         
      	]);

      	db.users.insertOne({
      	firstName : "Jane",
      	lastName: "Doe",
      	age: 21,
      	contact: {
      		phone: "1234567890",
      		email: "jandedoe@gmail.com"
      	},
      	course: ["CSS", "Javascript", "Python"],
      });
   
   db.users.insertOne({
      	firstName : "Jane",
      	lastName: "Doe",
      	age: 21,
      	contact: {
      		phone: "1234567890",
      		email: "jandedoe@gmail.com"
      	},
      	course: ["CSS", "Javascript", "Python"],
      	gender: "Female"
      });
  db.users.insertOne({
      	firstName : "Jane",
      	lastName: "Doe",
      	age: 21,
      	contact: {
      		phone: "1234567890",
      		phone: "1234567890123",
      		email: "jandedoe@gmail.com"
      	},
      	course: ["CSS", "Javascript", "Python"],
      	gender: "Female"
      });

  //[SECTION] FIND (READ)  
        /*
             Syntax:
             // it will show us all the documents in our collection
               db.collectionName.find();
               //it will show us all the documents that has the given fieldset
              db.collectionName.finde({field: value})



        */

  db.users.find();

  db.users.find({
  	age: 76
  });
  db.users.find({
  	firstName: "jane"
  });

  db.users.find({
  	firstName: "jane"
  });

  //finding documents using multiple fieldsets

  /*

    Syntax:
    db.collectionName.find({fieldA:valueA, fieldB: valueB})


  */


  db.users.find({
  	lastName:"Armstrong",
  	age: 82
  })

  //[SECTION] Updating Documents 

  //add document

  db.users.insertOne({
  	firstName:"Test",
  	lastName: "Test",
  	age: 0,
  	contact: {
  		phone: "0000000",
  		email: "test@gmail.com"
  	},
  	course: [],
  	department: "none"
  })

  //updateOne

    /*
        Syntax:
        db.collectionName.updateOne({criteria}, {$set: {field:value}}),


    */

  db.users.updateOne({
  	firstName: "Jane"},
  	{
  		$set:{
  			lastName:"Hawking";
  		}
  	})

      //updating multiple documents
      /*
              do.collectioName.updateMany({Criteraia},
                $set: {
	              field:value;
                }

              )
      */

  db.users.updateMany({firstName: "Jane"},
{
	$set: {
lastName: "Wick",
age: 25
}
}
  	)

  db.users.updateMany({department: "none"},{
  	$set: {
  		department: "HR"
  	}
  })

  //replacing the old document
  /*

       db.users.replaceOne({criteria}, {document/objectToReplace})

  */

   db.users.replaceOne({firstName: "Test"}, {
   	firstName: "Kenneth",
   	lastName: "Mortel"
   })

   //[SECTION] DELETING documents

   /*

       //deleting single document

       db.collectionName.deleteOne({criteria});

   */

   db.users.deleteOne({firstName: "Jane"});
        //Delete many

   /*

         Syntax:
         db.collectionName.deleteMany({criteria})

   */

   db.users.deleteMany({firstName: "Jane"})

    //reminder 
     //db.collectionName.deleteMany()
     // Remind: dont forget to ad criteria

   //[SECTION] Advanced Queries

      //embedded -  object
      // nested field - array

   //query on an embedded 
   db.users.find({"contact.phone": "123456"})

   //query an array with exact elements
   db.users.find(courses:["CSS","Javascript", "Python"])

   //queryin an array without regards to order and elements
   db.users.find({courses: {$all: ["React"]}})